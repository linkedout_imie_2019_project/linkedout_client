import { NgModule }                  from '@angular/core';
import { BrowserModule }             from '@angular/platform-browser';
import { NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent }     from './app.component';
import { SigninComponent }  from './signin/signin.component';

@NgModule({
    declarations: [
        AppComponent,
        SigninComponent
    ],
    imports     : [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        NgbAlertModule
    ],
    providers   : [],
    bootstrap   : [AppComponent]
})
export class AppModule {
}
